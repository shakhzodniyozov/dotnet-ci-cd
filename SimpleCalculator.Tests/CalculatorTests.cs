using Xunit;

namespace SimpleCalculator.Tests;

public class CalculatorTests
{
    [Fact]
    public void Add()
    {
        var result = Calculator.Add(1, 1);

        Assert.Equal(2, result);
    }

    [Fact]
    public void Subtract()
    {
        var result = Calculator.Subtract(2, 1);

        Assert.Equal(1, result);
    }

    [Fact]
    public void Multiply()
    {
        var result = Calculator.Multiply(3, 1);

        Assert.Equal(3, result);
    }

    [Fact]
    public void Divide()
    {
        var result = Calculator.Divide(4, 2);

        Assert.Equal(2, result);
    }
}