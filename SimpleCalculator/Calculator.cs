public class Calculator
{
    public static double Add(int a, int b) => a + b;
    public static double Subtract(int a, int b) => a - b;
    public static double Multiply(int a, int b) => a * b;
    public static double Divide(int a, int b) => a / b;
}